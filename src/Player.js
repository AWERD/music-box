import React, { Component } from 'react';
import './Player.css';
import holly from './holly.jpg';

class Player extends Component {

	constructor(props) {
		super(props);
		this.state = { 
			isPlaing: true,
			file: 'http://music-box/audio_file.mp3',
			progress: 0.0,
			volume: 1.0
		 };

		 this.state.progress_dirty = false;
	}

	

	play() {
		if (this.state.isPlaing === true) {
  		this.refs.audio.play();
  	} else if (this.state.isPlaing === false){
  		this.refs.audio.pause();
  	}
  	this.setState({ isPlaing: !this.state.isPlaing });
	}

	next() {
		this.setState({ file: 'http://music-box/01_korol_i_shut_dobrie_ludi_hor_nishih_myzuka.me.mp3' });
		
	}

	setProgress(evt) {
		var progress = (evt.clientX - offsetLeft(this.refs.progress_bar)) / this.refs.progress_bar.clientWidth;
		this.setState({
		 progress: progress
		  });
		this.state.progress_dirty = true;
	}

	setVolume(evt) {
		var volume = (evt.clientX - offsetLeft(this.refs.volume_bar)) / this.refs.volume_bar.clientWidth;
		this.setState({
		 volume: volume
		  });
		this.refs.audio.volume = this.state.volume;
	}


  render() {

  	let play;

  	if (this.state.isPlaing === true) {
  		play = <i className="fa fa-play" aria-hidden="true"></i>;
  	} else if (this.state.isPlaing === false) {
  		play = <i className="fa fa-pause" aria-hidden="true"></i>;
  	}

  	if (this.refs.audio) {
  		if (this.state.progress_dirty) {
  			this.state.progress_dirty = false;
  			this.refs.audio.currentTime = this.refs.audio.duration * this.state.progress;
  		}
  		if (this.refs.audio.currentSrc !== this.state.file) {
  			this.refs.audio.src = this.state.file;
  			this.refs.audio.play();
  		}
  		
  	}



    return (
      <div className="content">
      	<audio ref="audio"> 
						<source src={ this.state.file }/>
					</audio>
      	<div className="col-12 col-xs-12 col-sm-6 col-md-4 col-lg-4">
      		<div className="plaing-block">
      			<div className="plaing-top">
	      			<div className=""><img src={holly} className="plaing-image" alt="album" /></div>
							<div ref="progress_bar" className="bar" onClick={ this.setProgress.bind(this) }>
		      	 		<div  className="progress-bar" style={{ width: (this.state.progress) * 100 + '%'}}></div>
		      	 	</div>
      			</div>
						<div className="plaing-content">
								<div className="plaing-desc">
								<div className="plaing-title"><span>Праздник крови</span></div>
								<div className="plaing-album"><span>TOOD</span><span> - </span><span>Король и Шут</span></div>
								<div className="plaing-artist"></div>
								<div className="controls">
			      	 	<div><button className="player-button prev-button"><i className="fa fa-step-backward" aria-hidden="true"></i></button></div>
			      	 	<div><button className="player-button play-button" onClick={ this.play.bind(this) }>{play}</button></div>
			      	 	<div><button className="player-button next-button" onClick={ this.next.bind(this) }><i className="fa fa-step-forward" aria-hidden="true"></i></button></div>
			      	 </div>
							</div>
							<div className="volume-block">
								<div><i className="fa fa-volume-up" aria-hidden="true"></i></div>
								<div ref="volume_bar" className="volume" onClick={ this.setVolume.bind(this) }>
			      	 		<div  className="volume-bar" style={{ width: (this.state.volume) * 100 + '%'}}><div className="volume-curcle"></div></div>
			      	 	</div>
							</div>
						</div>
					</div>
      	</div>
      	<div className="col-12 col-xs-12 col-sm-6 col-md-8 col-lg-8">
	      	<div className="playlist-container">
	      		<ul className="playlist">
	      			<li className="playlist-item"><a href="">
	      				<div className="flex-item">
	      					<div className="image-block">
	      						<div className="song-image"></div>
	      					</div>
	      					<div className="song-desc">
	      						<div className="song-title">Праздник крови</div>
	      						<div className="song-album">TOOD</div>
	      						<div className="song-artist">Король и Шут</div>
	      					</div>
	      				</div>
	      			</a></li>
	      			<li className="playlist-item"><a href="">
	      				<div className="flex-item">
	      					<div className="image-block">
	      						<div className="song-image"></div>
	      					</div>
	      					<div className="song-desc">
	      						<div className="song-title">Пирожки от Лаввет</div>
	      						<div className="song-album">TOOD</div>
	      						<div className="song-artist">Король и Шут</div>
	      					</div>
	      				</div>
	      			</a></li>
	      			<li className="playlist-item"><a href="">
	      				<div className="flex-item">
	      					<div className="image-block">
	      						<div className="song-image"></div>
	      					</div>
	      					<div className="song-desc">
	      						<div className="song-title">Праздник крови</div>
	      						<div className="song-album">TOOD</div>
	      						<div className="song-artist">Король и Шут</div>
	      					</div>
	      				</div>
	      			</a></li>
	      		</ul>
	      	</div>
	      </div>
      </div>
    );
  }
}

function offsetLeft(el) {
	var left = 0;
	while (el && el !== document) {
		left += el.offsetLeft;
		el = el.offsetParent;
	}
	return left;
}


export default Player;
