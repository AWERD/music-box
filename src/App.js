import React, { Component } from 'react';
import logo from './logo.png';
import './App.css';

import Player from './Player';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <div className="App-logo-block">
            <img src={logo} className="App-logo" alt="logo" />
            <span><h1>Music-Box</h1></span>
          </div>
        </div>
        <p className="App-intro">
          <Player />
        </p>
      </div>
    );
  }
}

export default App;
